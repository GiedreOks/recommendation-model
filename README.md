# Recommendation model

This model was a part of university project. 

The goal of the project was to create a recommendation engine with a baseline model. The baseline model used is described below:

> **rating = population mean + user bias + item bias**

*recommenderlab()*, Item based and User based collaborative filtering algorithms were used for the recommendations.
